# Chat Bot
descp......

## React libs
- react-bootstrap
- react-toastify
- react-chat-ui

## To run the app
* clone the repo
* `npm start` on the terminal

## Folder Structure

```
my-app/
  README.md
  node_modules/
  package.json
  public/
    index.html
  src/
  	components/
	  chatUI/
		chatUI.js
		chatUI.css
    App.css
    App.js
    index.css
    index.js
```