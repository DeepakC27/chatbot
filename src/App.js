import React, { Component } from 'react';
import './App.css';
import ChatUI from './components/chatUI/chatUI'
import { ToastContainer, toast } from 'react-toastify';

class App extends Component {
  render() {
    return (
      <div>
        <ToastContainer />
        <div className='navbar mb20'>
          <img />
          <span>App Name</span>
        </div>
        <p className='app-descp mb20'>App descp here....</p>
        <ChatUI />
        <p className='mb20 mt20 about-us'>About Us....</p>
      </div>
    );
  }
}

export default App;
