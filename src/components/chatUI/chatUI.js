import React, { Component } from 'react';
// import ChatBubble from 'react-chat-bubble';
import { ChatFeed, Message } from 'react-chat-ui'
import { toast } from 'react-toastify';
import './chatUI.css';
import { sampletestAsync, botResponseAsync } from './APIRequest'

class ChatUI extends Component {
  constructor (props) {
    super(props)
    this.state = {
      is_typing: false,
      inputValue: '',
      chatContent: [
        new Message({
          id: 1,
          message: "Hello, how may I assist you?",
        })
      ]
    }
  }

  handleOnChange = (holder, e) => {
    this.setState({
      inputValue : e.target.value
    })
  }

  handleUserInput = (e) => {
    let flg =false
    if (e.which === 13){
        flg = true
    } else {
      flg = false
    }
    this.setState({
      is_typing: flg
    })
  }

  onMessageSubmit = (e) => {
    const msg = this.state.inputValue;
    e.preventDefault();
    var removeSpace = /^[\s]*$/
    console.log(msg.replace(removeSpace,'').length)
    if (msg.replace(removeSpace,'').length <= 0) {
      this.setState({
        is_typing: false
      })
      toast.error('Plz enter a value text!!!!!', {
        autoClose: 2000,
        closeButton: false
      })
      return false;
    }
    this.pushMessage(0, msg);
    this.setState({
      inputValue: ''
    })
    // fetch data via api
    botResponseAsync({ 'uid':1, 'query': msg})
    .then((response) => {
      this.pushMessage(1, response.toString())
      this.setState({
        is_typing: false,
      })
    })

    // remove this block
    setTimeout(() => {
      let response = sampletestAsync({ 'uid':1, 'query': msg})
      this.pushMessage(1, response)
      this.setState({
        is_typing: false,
      })
    }, 1000)
    //
    return true;
  }

  pushMessage = (recipient, message) => {
    const msgContent = this.state.chatContent;
    const newMessage = new Message({
      id: recipient,
      message,
    });
    msgContent.push(newMessage);
    this.setState({
      chatContent: msgContent
    });
  }

  render() {
    return (
      <div className="chat-component mb20 mt20 mlr">
        <ChatFeed
          maxHeight={250}
          isTyping={this.state.is_typing}
          messages={this.state.chatContent}
        />
        <form onSubmit={e => this.onMessageSubmit(e)}>
          <input className='message-input'
            ref='msgVal' onKeyPress={this.handleUserInput} onChange={this.handleOnChange.bind(null, this)}
            value={this.state.inputValue}
            placeholder="Type a message..." />
        </form>
      </div>
    );
  }
}

export default ChatUI;
