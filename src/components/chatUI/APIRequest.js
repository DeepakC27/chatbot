import axios from 'axios';
import { toast } from 'react-toastify';

export const botResponseAsync = (requestObj) => {
  axios.post('http://localhost:8000/talk', requestObj)
  .catch((error) => {
    console.log(error);
    toast.error('Network error', {
      autoClose: 2000,
      closeButton: false
    })
  })
  .then((response) => {
    return response
  });
}

export const sampletestAsync = (obj) => {
  let body = JSON.stringify(
    ...obj
  );
  return 'Hi this is the bot...'
}

// export default botResponseAsync
